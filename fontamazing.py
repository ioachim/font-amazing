#! env python
# coding: utf-8

import re, argparse, sys, os
import yaml
import jinja2

parser = argparse.ArgumentParser('Generate a Font Awesome icon list based on a jinja template')
parser.add_argument('output', help='File to output')
parser.add_argument('-f', '--format', help='Template to use')

args = parser.parse_args()
args.format = args.format or os.path.splitext(args.output)[1][1:]

env = jinja2.Environment(loader=jinja2.FileSystemLoader('./templates'))

if not args.format:
    print('error: template format is required (missing file extension or -f option)')
    sys.exit(1)

try:
    template = env.get_template('{0}.jinja'.format(args.format))
except jinja2.TemplateNotFound:
    print('error: template "{0}" not found'.format(args.format))
    sys.exit(1)

icons = yaml.load(open('./Font-Awesome/src/icons.yml'))['icons']

wordseparator = re.compile('[-_ ]+')
env.filters['camelcase'] = lambda s: ''.join(map(str.capitalize, wordseparator.split(s)))

code = template.render(icons=icons)

open(args.output, 'w').write(code)


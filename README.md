# Font Amazing

A simple tool for generating Font Awesome icon list based on Jinja2 templates

# Requirements

- Python 3.x
- PyYAML
- Jinja2

